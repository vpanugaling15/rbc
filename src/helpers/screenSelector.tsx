import frameOneTwo from "../assets/frame-1-and-2.jpg";
import frame3a from "../assets/frame-3a.jpg";
import frame3b from "../assets/frame-3b.jpg";
import frame3c from "../assets/frame-3c.jpg";
import frame3d from "../assets/frame-3d.jpg";
import lastFrame from "../assets/last-frame.jpg";

export enum ScreenImage {
  firstScreen = "First Screen",
  secondScreen = "Second Screen",
  thirdScreenA = "Third Screen A",
  thirdScreenB = "Third Screen B",
  thirdScreenC = "Third Screen C",
  thirdScreenD = "Third Screen D",
}

export const screenSelector = (screen: ScreenImage) => {
  if (
    screen === ScreenImage.firstScreen ||
    screen === ScreenImage.secondScreen
  ) {
    return frameOneTwo;
  } else if (screen === ScreenImage.thirdScreenA) {
    return frame3a;
  } else if (screen === ScreenImage.thirdScreenB) {
    return frame3b;
  } else if (screen === ScreenImage.thirdScreenC) {
    return frame3c;
  } else if (screen === ScreenImage.thirdScreenD) {
    return frame3d;
  } else {
    return lastFrame;
  }
};
