import "../App.css";

const Footer = () => {
  return (
    <div className="footer">
      <p className="text-blue text-xs disclaimer-text">
        Your responses are being collected for consumer research purposes and
        will not be shared with third parties.
      </p>
    </div>
  );
};

export default Footer;
