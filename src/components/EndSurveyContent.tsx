import "../App.css";
import Footer from "./Footer";
import logo from "../assets/RBC-logo.png";

const EndSurveyContent = () => {
  return (
    <div>
      <h1 className="text-blue text-xl">Thanks for completing our survey.</h1>
      <div className="message-bubble bg-blue fit-content">
        <strong>Learn more on how RBC can help</strong>
      </div>
      <img
        alt="logo"
        className="absolute right-0 bottom-8"
        src={logo}
        height="55"
        width="55"
      />
      <Footer />
    </div>
  );
};

export default EndSurveyContent;
