import React, { useState, useEffect } from "react";
import cx from "classnames";
import { has, last, isEmpty } from "lodash";
import logo from "./assets/RBC-logo.png";
import "./App.css";
import data from "./data.json";
import { screenSelector, ScreenImage } from "./helpers/screenSelector";

import Footer from "./components/Footer";
import EndSurveyContent from "./components/EndSurveyContent";

function App() {
  const [start, setStart] = useState<boolean>(false);
  const [end, setEnd] = useState<boolean>(false);
  const [background, setBackground] = useState(
    screenSelector(ScreenImage.firstScreen)
  );
  const [answers, setAnswers] = useState<any>();
  const [question, setQuestion] = useState(data.first.question);
  const [surveyData, setSurveyData] = useState<any>([]);
  const [tempStore, setTempStore] = useState<any>([]);
  const [animateFadeDown, setAnimateFadeDown] = useState("");

  useEffect(() => {
    setAnimateFadeDown("");
    setTimeout(() => {
      setAnimateFadeDown("animate-fade-in-down");
    }, 10);
  }, [answers]);

  const handleAnswer = (current: any) => {
    setBackground(screenSelector(current.screen));
    setAnswers(current.answers);
    setQuestion(current.question);

    setTempStore([...tempStore, current]);

    if (isEmpty(current.answers)) {
      setSurveyData([
        ...surveyData,
        {
          question: current.question,
          answer: current.listItem,
        },
      ]);
      setEnd(true);
    }

    if (!(has(current, "answers") && current.answers.length)) {
      const lastItem: any = last(tempStore);

      if (!isEmpty(lastItem)) {
        setSurveyData([
          ...surveyData,
          {
            question: lastItem.question,
            answer: current.listItem,
          },
        ]);
      }
    }

    // End the survey
    !has(current, "answers") && setEnd(true);
  };

  const handleStartSurvey = () => {
    setStart(true);
    setAnswers(data.first.answers[0].answers);
    setQuestion(data.first.answers[0].question);
    setSurveyData([
      ...surveyData,
      {
        question: data.first.question,
        answer: "Yes",
      },
    ]);
  };

  const handleEndSurvey = () => {
    setSurveyData([
      ...surveyData,
      {
        question: data.first.question,
        answer: "No",
      },
    ]);
    setEnd(true);
  };

  return (
    <div className="container mx-auto">
      <div className="flex justify-center items-center h-screen">
        <main
          className="relative billboard"
          style={{
            background: `url(${background})`,
          }}
        >
          <section
            className="absolute z-0 right-7 top-6"
            style={{ width: 500, height: 200 }}
          >
            {end ? (
              <EndSurveyContent />
            ) : (
              <React.Fragment>
                <div className={cx("flex", start && "float-left")}>
                  <img alt="logo" src={logo} height="55" width="55" />
                  <div className="message-bubble bg-blue">{question}</div>
                </div>

                {!isEmpty(answers) ? (
                  <div className={cx(animateFadeDown)}>
                    <div className="flex items-end flex-col">
                      {answers.map((answer: any, index: number) => {
                        return (
                          <div
                            key={`list=${index}`}
                            className="text-blue bg-white bubble py-1 px-2 mb-2 "
                            onClick={() => handleAnswer(answer)}
                          >
                            {answer.listItem}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                ) : (
                  <div className="flex justify-end">
                    <button
                      onClick={() => handleStartSurvey()}
                      className="mr-2 bg-white hover:bg-blue hover:text-white text-blue font-bold py-2 px-12 text-sm"
                    >
                      YES
                    </button>
                    <button
                      onClick={() => handleEndSurvey()}
                      className="bg-white hover:bg-blue hover:text-white text-blue font-bold py-2 px-12 text-sm"
                    >
                      NO
                    </button>
                  </div>
                )}
              </React.Fragment>
            )}

            {!start && <Footer />}
          </section>
        </main>
      </div>
    </div>
  );
}

export default App;
